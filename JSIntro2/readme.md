# JSIntro II

Welcome to the exercise files for Javascript Introduction II. There are 3 files in this module: demo.js, demo.html and library.js. We'll need to source the demo.js file in the demo.html file and create user interactions. We will be using demo.js to practice DOM manipulation and JS events. In the library.js file, we'll be practicing DOM manipulation with the jQuery libraray. Make sure you review JS data ype, logic and types of functions we talked about in JSIntro I. There are also exercises in the exercise folder. Complete the challenges to master to JS skills!

Psst: check out solution branch for solutions
Link to Slides: https://docs.google.com/presentation/d/1VhL-JzCT7r3WxI2vfGuTUgAP4WVkcJlV3J-RpgVqKmg/edit?usp=sharing